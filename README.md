# OpenML dataset: blogger

https://www.openml.org/d/1463

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**:   
**Source**: UCI
**Please cite**:   

Source:
 
http://www.ijcaonline.org/archives/volume47/number18/7291-0509
 
 
Data Set Information:
 
In this paper, we look for to recognize the causes of users tend to cyber space in Kohkiloye and Boyer Ahmad Province in Iran. Collecting information to form database is done by questionnaire. This questionnaire is provided as oral, written and also programming of a website which includes an internet questionnaire and the users can answer the questions as they wish. They entered their used websites, blogs and social networks during the day. After collecting questionnaires, the wed addresses are gathered to get expected results. And finally, their trustfulness is checked by analyzing their used web pages. As the results were same, for getting better and noiseless response, they will put in database.
  
Attribute Information:
 
We considered the following parameters as questions: age,  education, political attitudes, blog topic, and the type of the identity in internet, the influence of manager inefficiency on tendency, the effect of inefficient media on tendency, the effects of social and political conditions on tendency and finally the effect of poverty in the province on tendency. The noisy or too detailed data in database makes us far from to get proper and suitable answers of algorithms [8]. We preprocessed the data and eliminated some non-relevant data. Finally the followings are considered as the main fields which include: education, political caprice, topics, local media turnover (LMT) and local, political and social space (LPSS). 

The collected data are shown in Table 1. In order to get correct answer, we classify bloggers to two groups: professional bloggers and seasonal (temporary) bloggers. Professional bloggers are those who adopt blog as an effective digital media and interested in digital writing in continuous time intervals. Seasonal (temporary) bloggers are not professional and follow blogging in discrete time periods. In this study, we review the tendency factors considering whether these people are among professional bloggers (Pro Bloggers, PB) and then, consider the other factors according to it.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/1463) of an [OpenML dataset](https://www.openml.org/d/1463). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/1463/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/1463/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/1463/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

